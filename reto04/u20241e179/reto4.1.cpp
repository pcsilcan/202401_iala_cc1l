#include <iostream>
#include <ctime>

using namespace std;

int num_r(int min, int max) {
	return rand() % (max - min + 1) + min;
}
void create_array() {
	int num = num_r(100, 500);
	int* array = new int[num];
	for (int i = 0; i < num; ++i) {
		array[i] = num_r(1, 10000);
	}
	cout << "Numero de elementos: " << num << endl;
	for (int i = 0; i < num; ++i) {
		cout << i + 1 << "." << array[i] << endl;
	}
	delete array;

}


int main() {
	srand(time(nullptr));
	create_array();
	system("pause");
	return 0;
}