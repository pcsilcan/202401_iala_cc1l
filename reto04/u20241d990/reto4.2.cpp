#include <iostream>
#include <conio.h>
using namespace std;

int* crearMatriz(int n, int m);
void imprimirMatriz(int* matriz, int n, int m);


int main() {

	int n, m;

	cout << "Ingresa el numero de filas: " << endl;
	cin >> n;
	cout << "Ingresa el numero de columnas: " << endl;
	cin >> m;

	//inicializar matriz despues owo
	int* matriz = crearMatriz(n, m);


	cout << "Matriz de " << n << " x " << m << " inicializada con ceros:" << endl;

	imprimirMatriz(matriz, n, m);

	delete[] matriz;

	_getch();
	return 0;
}

int* crearMatriz(int n, int m) {

	int* matriz = new int[n * m];

	for (int i = 0; i < n * m; ++i) {
		matriz[i] = 0;
	}
	return matriz;
}
void imprimirMatriz(int* matriz, int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			cout << matriz[i * m + j] << " "; //espacio para q se vea bonito
		}
		cout << endl;
	}
}
